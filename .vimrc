set nocompatible

" Plugins {{{

" Use Vim Plug to manage plugins
" Install it with follow
" $ curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
"     https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

call plug#begin()

" Status line and tab line
Plug 'vim-airline/vim-airline' 
Plug 'vim-airline/vim-airline-themes'
" Easy motion with leader
Plug 'easymotion/vim-easymotion'
" Git plugin
Plug 'tpope/vim-fugitive'
" NERD Tree
Plug 'scrooloose/nerdtree'
" Code commenter
Plug 'scrooloose/nerdcommenter'
" SuperTab
Plug 'ervandew/supertab'
" Provides automatic closing of quotes, parenthesis, brackets, etc.
Plug 'Raimondi/delimitMate'
" Python mode
Plug 'klen/python-mode'
" Autocopletion
Plug 'Valloric/YouCompleteMe', { 'do': './install.py --tern-completer' }
" Theme
Plug 'altercation/vim-colors-solarized'
" Class/module browser
" Plug 'majutsushi/tagbar'
" Code and files fuzzy finder
Plug 'ctrlpvim/ctrlp.vim'
" Consoles as buffers
Plug 'rosenfeld/conque-term'
" Snippets
Plug 'SirVer/ultisnips'
" Snippets themself
Plug 'honza/vim-snippets'
" Pending tasks list
Plug 'fisadev/FixedTaskList.vim'
" Twig highlighting
" Plug 'evidens/vim-twig'
" Jinja2 templates highlighting
Plug 'mitsuhiko/vim-jinja'
" Toggle list and quickfix windows
Plug 'Valloric/ListToggle'
" Syntax checking
Plug 'scrooloose/syntastic'
" Syntax highlighting
Plug 'hdima/python-syntax'
" Json support
Plug 'helino/vim-json'
" Javascript support
Plug 'pangloss/vim-javascript'

call plug#end()
" }}}
" Colors {{{
syntax enable           " enable syntax processing
set t_Co=256
let g:solarized_termcolors=256
set background=dark
colorscheme solarized
" }}}
" Misc {{{
set ttyfast                     " faster redraw
set lazyredraw
set backspace=indent,eol,start
" automatically change window's cwd to file's dir
set autochdir
" }}}
" Spaces & Tabs {{{
set tabstop=4           " 4 space tab
set expandtab           " use spaces for tabs
set softtabstop=4       " 4 space tab
set shiftwidth=4
set modelines=1
filetype indent on
filetype plugin on
set autoindent
" }}}
" UI Layout {{{
set number              " show line numbers
set showcmd             " show command in bottom bar
set nocursorline          " highlight current line
set wildmenu
set showmatch           " higlight matching parenthesis
" when scrolling, keep cursor 3 lines away from screen border
set scrolloff=3
" autocompletion of files and commands behaves like shell
" (complete only the common part, list the options that match)
set wildmode=list:longest
" }}}
" Searching {{{
set ignorecase          " ignore case when searching
set incsearch           " search as characters are entered
" set hlsearch            " highlight all matches
" }}}
" Folding {{{
set foldmethod=indent   " fold based on indent level
set foldnestmax=10      " max 10 depth
set foldenable          " don't fold files by default on open
nnoremap <space> za
set foldlevelstart=10    " start with fold level of 1
" }}}
" Line Shortcuts {{{
" move through windows
map <C-l> <c-w>l
map <C-h> <c-w>h
map <C-k> <c-w>k
map <C-j> <c-w>j
" move vertically by visual line
nnoremap j gj
nnoremap k gk
" Tab shortcuts 
nnoremap <C-1> <Esc>1gt
nnoremap <C-2> <Esc>2gt
nnoremap <C-3> <Esc>3gt
" show tree
map <F2> :NERDTreeToggle<CR>
" show pending tasks list
map <F4> :TaskList<CR>
" save as sudo
ca w!! w !sudo tee "%"
" }}}
" Leader Shortcuts {{{
let mapleader=","
map <Leader>p "*p
" Run python aiohttp web app
map <leader>r :NERDTreeFocus<CR>:ConqueTermTab make run<CR>
" Run python tests for aiohttp web app
map <leader>t :NERDTreeFocus<CR>:ConqueTermTab make test<CR>
" Save session
nnoremap <leader>s :mksession<CR>
" Save file
map <leader>w :w<CR>
" quit file
map <leader>x :q<CR>
" }}}
" Plugins Options {{{
" Airline {{{
" let g:airline#extensions#tabline#left_sep = ' '
" let g:airline#extensions#tabline#left_alt_sep = '|'
" let g:airline_left_sep = ''
" let g:airline_left_alt_sep = ''
" let g:airline_right_sep = ''
" let g:airline_right_alt_sep = ''
let g:airline_powerline_fonts = 1
set laststatus=2
" }}}
" CtrlP {{{
let g:ctrlp_match_window = 'bottom,order:ttb'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 0
let g:ctrlp_custom_ignore = '\vbuild/|dist/|venv/|target/|\.git/\.(o|swp|pyc|egg)$'
" }}}
" NERDTree {{{
let NERDTreeIgnore = ['\.pyc$', '\.pyo$', '^__pycache__', 'build', 'venv', 'egg', 'egg-info/', 'dist', 'docs']
" }}}
" Syntastic {{{
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
" let g:syntastic_check_on_open = 0
" let g:syntastic_check_on_wq = 0

let g:syntastic_python_checkers = ['flake8']
" let g:syntastic_python_pep8_args = '--ignore=E501'
" let g:syntastic_python_flake8_args='--ignore=E501'
let g:syntastic_ignore_files = ['.java$']

let g:syntastic_javascript_checkers = ['jshint', 'jscs']
" }}}
" UltiSnips {{{
let g:UltiSnipsExpandTrigger="<c-e>"
let g:UltiSnipsJumpForwardTrigger="<c-e>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
" }}}
" ConqueTerm {{{
" disable start messages 
let g:ConqueTerm_StartMessages = 0
" }}}
" PythonMode {{{
" Activate rope
" Keys:
" K             Show python docs
" <Ctrl-Space>  Rope autocomplete
" <Ctrl-c>g     Rope goto definition
" <Ctrl-c>d     Rope show documentation
" <Ctrl-c>f     Rope find occurrences
" <Leader>b     Set, unset breakpoint (g:pymode_breakpoint enabled)
" [[            Jump on previous class or function (normal, visual, operator modes)
" ]]            Jump on next class or function (normal, visual, operator modes)
" [M            Jump on previous class or method (normal, visual, operator modes)
" ]M            Jump on next class or method (normal, visual, operatori modes)
let g:pymode = 1
" turn off default python options
let g:pymode_options = 0
let g:pymode_indent = 1
let g:pymode_folding = 0
let g:pymode_motion = 1
" Documentation
let g:pymode_doc = 1
let g:pymode_doc_bind = 'K'
" Support virtualenv
let g:pymode_virtualenv = 1
" run code
let g:pymode_run = 0
let g:pymode_run_bind = '<leader>r'
" Enable breakpoints plugin
let g:pymode_breakpoint = 1
let g:pymode_breakpoint_bind = '<leader>b'
" Linting
let g:pymode_lint = 0
" syntax highlighting off
let g:pymode_syntax = 0
" Turn off rope
let g:pymode_rope = 0
" }}}

" }}}
" AutoGroups {{{
augroup config_group
    autocmd!
    autocmd VimEnter * highlight clear SignColumn
    autocmd BufWritePre *.php,*.py,*.js,*.txt,*.hs,*.java,*.md,*.rb :call <SID>StripTrailingWhitespaces()
    autocmd BufEnter *.md setlocal filetype=markdown
    autocmd BufEnter *.cls setlocal filetype=java
    autocmd BufEnter *.zsh-theme setlocal filetype=zsh
    autocmd BufEnter Makefile setlocal noexpandtab
    autocmd BufEnter *.sh setlocal tabstop=2
    autocmd BufEnter *.sh setlocal shiftwidth=2
    autocmd BufEnter *.sh setlocal softtabstop=2
    autocmd FileType vim setlocal foldmethod=marker 
    autocmd BufEnter .vimrc setlocal foldlevel=0 
augroup END

augroup python_group 
	autocmd!
    autocmd FileType python let python_highlight_all=1
	" highlight characters past column 80
	autocmd FileType python highlight Excess ctermbg=DarkGrey guibg=Black
	autocmd FileType python match Excess /\%80v.*/
	autocmd FileType python set nowrap
    " run python file
    autocmd filetype python noremap <buffer> <F5> :w<CR>:!python %<CR>
    autocmd filetype python inoremap <buffer> <F5> <Esc>:w<CR>:!python %<CR>
augroup END

" }}}
" Backups {{{
set backup 
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp 
set backupskip=/tmp/*,/private/tmp/* 
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp 
set writebackup
" }}}
" Custom Functions {{{
function! ToggleNumber()
    if(&relativenumber == 1)
        set norelativenumber
        set number
    else
        set relativenumber
    endif
endfunc

" strips trailing whitespace at the end of files. this
" is called on buffer write in the autogroup above.
function! <SID>StripTrailingWhitespaces()
    " save last search & cursor position
    let _s=@/
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    let @/=_s
    call cursor(l, c)
endfunction

" }}}
